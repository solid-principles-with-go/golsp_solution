// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
/* -- ****************************************************************/
/* --                          drones_test.go                        */
/* --                                                                */
/* --  Descripcion: Contiene las pruebas unitarias del ejercicio     */
/* --                                                                */
/* --   @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/* --                                                                */
/* --  © 2022 - Mercado Libre - Desafio Tecnico SOLID                */
/* -- ****************************************************************/

package test

import (
	"testing"

	"gitlab.com/rodrigoghm/golsp_ejercicio/class/droneX"
	"gitlab.com/rodrigoghm/golsp_ejercicio/class/droneY"
	"gitlab.com/rodrigoghm/golsp_ejercicio/class/droneZ"
)

func volarDrones(g droneX.Drone) string {
	return g.Volar()
}

func TestVolandoDroneX(t *testing.T) {
	dr := droneX.DroneX{}
	dr.SetNombre("Zii")
	dr.SetModelo("X")
	expected := "[Volando] En el aire, Todo OK, Balanceo Automatico Habilitado ..."
	rec := volarDrones(&dr)
	if rec != expected {
		t.Errorf("Valor devuelto para comprobacion de Drone tipo X erroneo.\n Valor recibido ===> [%s] \n Valor Esperado ===> [%s]", rec, expected)
	}
}

func TestVolandoDroneY(t *testing.T) {
	dr := droneY.DroneY{}
	dr.SetNombre("CSJ")
	dr.SetModelo("Y")
	expected := "[Volando] Estoy Dando Vueltassssss ..."
	rec := volarDrones(&dr)
	if rec != expected {
		t.Errorf("Valor devuelto para comprobacion de Drone tipo Y erroneo.\n Valor recibido ===> [%s] \n Valor Esperado ===> [%s]", rec, expected)
	}
}

func TestVolandoDroneZ(t *testing.T) {
	dr := droneZ.DroneZ{}
	dr.SetNombre("DJI")
	dr.SetModelo("Z")
	expected := "[Volando] Estoy Dando La Pirueta en Z ..."
	rec := volarDrones(&dr)
	if rec != expected {
		t.Errorf("Valor devuelto para comprobacion de Drone tipo Z erroneo.\n Valor recibido ===> [%s] \n Valor Esperado ===> [%s]", rec, expected)
	}
}
