# Liskov Substitution Principle

Este Caso de Uso representa una aplicacion de manejo de drones.

El codigo que nos interesa esta en un error que se presente en el script de test en 'test/drones_test.go".

Los primeros 2 test son aprobados sin problemas. Sin embargo, no se esta cumpliendo el principio de Sustitución de Liskov. Averigue porque?

Refactorice el codigo corrigiendo este problema de diseño de software para que el test pueda avanzar sin problemas.